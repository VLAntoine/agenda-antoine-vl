# Agenda

## Lancer le programme

Cloner le projet. Dans le répertoire du projet lancer :

```
npm install
```

Démarrer le projet en lançant :

```
npm run serve
```
Rendez-sur _http://localhost:8080/_ pour utiliser le programme.

## Objectif du programme

Ce projet s'inscrit dans le cadre d'un cours de B3 à l'EPSI, sur l'UML. L'objectif était 
de réaliser, en un jour, un programme qui correspondait un diagramme de classe. Le choix de la techno 
était laissé libre. L'idée était de faire le POO. Des points étaient ajoutés si on ajoutait
une base de données, une connexion par API, une interface graphique, etc.

Nous avons choisi comme technologie Typescript et Vuejs avec pour idée de faire une interface
graphique.

## Fonctionnement et implémentation

En l'état, le programme permet de créer un utilisateur, de lui ajouter un agenda et d'en supprimer.
Il ne permet pas encore d'ajouter des contacts à chaque agenda et des adresses à chaque contact.

Le dossier _src/models_ contient toute la logique métier du programme, avec les différentes classes attendues.

Le dossier _src/views_ contient la page principale de l'application.

Le dossier _src/components_ contient différents composants _Vuejs_ appelés par la page principale.

Le diagramme de classe est disponible dans _src/assets_