export abstract class ContactDetail{
    public value: string;
    protected pattern: string;
    protected errorMessage: string;

    protected constructor(value: string, pattern: string, errorMessage: string) {
        this.pattern = pattern;
        this.errorMessage = errorMessage;
        if (this.validate(value)){
            this.value = "";
        } else {
            throw new Error(errorMessage);
        }
    }

    protected validate(inputValue: string) {
        return inputValue.match(this.pattern);
    }
}

export class Adress extends ContactDetail {
    constructor(value: string) {
        super(
            value,
            "^([0-9]*) ?([a-zA-Z,\\. ]*) ?([0-9]{5}) ?([a-zA-Z]*)$",
            ""
        );
    }
}

export class Phone extends ContactDetail {
    constructor(value: string) {
        super(
            value,
            "^(0|\\+33|0033)[1-9][0-9]{8}$",
            ""
        );
    }
}

export class Email extends ContactDetail {
    constructor(value: string) {
        super(
            value,
            "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$",
            ""
        );
    }
}

export class Website extends ContactDetail {
    constructor(value: string) {
        super(
            value,
            "^[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)\n$",
            ""
        );
    }
}