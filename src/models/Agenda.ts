import {Contact} from "./Contact";

export class Agenda {
    public name: string;
    public contacts: Contact[];

    constructor(name: string) {
        this.name = name;
        this.contacts = [];
    }

    public addContact(contact: Contact){
        this.contacts.push(contact);
    }

    public removeContact(index: number){
        this.contacts.splice(index, 1);
    }
}