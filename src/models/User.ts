import {Agenda} from "./Agenda";

export class User {
    public login: string;
    public agendas: Agenda[];
    private password: string;
    private jwtToken: string;

    constructor(login: string, password: string) {
        this.login = login;
        this.agendas = [];
        this.password = password;
        this.jwtToken = "";
    }

    public addAgenda(agenda: Agenda){
        this.agendas.push(agenda);
    }

    public removeAgenda(index: number){
        this.agendas.splice(index, 1);
    }
}