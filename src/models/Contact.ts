import {ContactDetail} from "./ContactDetail";

export class Contact {
    public name: string;
    public addresses: ContactDetail[];

    public constructor(name: string) {
        this.name = name;
        this.addresses = [];
    }

    public addAddress(address: ContactDetail){
        this.addresses.push(address);
    }

    public removeAdress(index: number){
        this.addresses.splice(index, 1);
    }
}